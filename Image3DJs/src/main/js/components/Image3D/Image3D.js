/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import sensor from '@ohos.sensor';

export default {
    props: {
        scale: 1.2, // 图片放大比例
        sensitivity: 1, // 灵敏度
        imgBackground: '', // 下层图片
        imgMid: '', // 中层图片
        imgForeground: '', // 上层图片
    },
    data: {
        translateX: 0,
        translateY: 0,
        comWidth: 0,
        comHeight: 0,
        maxTranslateX: 0,
        maxTranslateY: 0,
        firstRetGamma: 0,
        firstRetBeta: 0
    },
    computed: {
        translateXBackground() {
            return this.translateX;
        },
        translateXForeground() {
            return -this.translateX;
        },
        translateYBackground() {
            return this.translateY;
        },
        translateYForeground() {
            return -this.translateY;
        },
    },
    onInit() {
        // 设置默认值
        this.scale = this.scale === undefined ? 1.2 : this.scale;
        this.sensitivity = this.sensitivity === undefined ? 1 : this.sensitivity;
    },
    onReady() {
        var that = this;
        // 观察设备方向传感器数据变化
        sensor.on(sensor.SensorType.SENSOR_TYPE_ID_ORIENTATION, function (data) {
            // 记录第一次获取的方向数据
            if (that.firstRetBeta === 0) {
                that.firstRetGamma = data.gamma;
                that.firstRetBeta = data.beta;
            }
            // 位移距离 = (当前的方向数据 - 第一次记录的数据) * 灵敏度
            that.setTranslateX((data.gamma - that.firstRetGamma) * that.sensitivity * 0.7);
            that.setTranslateY((data.beta - that.firstRetBeta) * that.sensitivity * 0.7);
        }, {
            interval: 100000000
        });
    },
    onPageShow() {
        // 获取组件尺寸
        this.comWidth = parseInt(this.$refs.stack_image_3d.getBoundingClientRect().width * this.scale, 10);
        this.comHeight = parseInt(this.$refs.stack_image_3d.getBoundingClientRect().height * this.scale, 10);
        // 计算最大可位移距离
        this.maxTranslateX = this.comWidth * (this.scale - 1) / 2 / 2;
        this.maxTranslateY = this.comHeight * (this.scale - 1) / 2 / 2;
    },
    onDestroy() {
        // 取消订阅设备方向传感器数据
        sensor.off(sensor.SensorType.SENSOR_TYPE_ID_ORIENTATION, null);
    },
    setTranslateX(translateX) {
        // 如果位移距离超过最大可位移距离，则只位移最大可位移距离
        if (translateX < -this.maxTranslateX) {
            this.translateX = -this.maxTranslateX;
        } else if (translateX > this.maxTranslateX) {
            this.translateX = this.maxTranslateX;
        } else {
            this.translateX = translateX;
        }
    },
    setTranslateY(translateY) {
        // 如果位移距离超过最大可位移距离，则只位移最大可位移距离
        if (translateY < -this.maxTranslateY) {
            this.translateY = -this.maxTranslateY;
        } else if (translateY > this.maxTranslateY) {
            this.translateY = this.maxTranslateY;
        } else {
            this.translateY = translateY;
        }
    },
};



